class Seller:
    def __init__(self, company, link, phone, year, price, address):
        self.company_name = company
        self.link = link
        self.phone = phone
        self.address = address
        self.summary_price = price
        self.summary_year = year
        self.car_quantity = 1

    def get_company(self):
        return self.company_name

    def add_car(self, year, price):
        self.car_quantity += 1
        self.summary_year += year
        self.summary_price += price

    def __eq__(self, other):
        return self.company_name == other.company_name

    def get(self):
        return {
            'nazwa_firmy': self.company_name,
            'adres': self.address,
            'link': self.link,
            'telefon': self.phone,
            'ilosc samochodow': self.car_quantity,
            'sredni rok': self.summary_year / self.car_quantity,
            'srednia cena': self.summary_price / self.car_quantity
        }


