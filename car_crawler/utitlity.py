import sys

import requests
from bs4 import BeautifulSoup


def get_page(number):
    home = requests.get('https://en.autogidas.lt/skelbimai/automobiliai/?f_50=kaina_asc&page={}'.format(number))
    return BeautifulSoup(home.text, 'html.parser')


def get_detail_page(link):
    """Link should be in format
    /skelbimas/audi-a6-dyzelinas--2005-y-c6-tdi--is-prancuzijos-0131489936.html
    """
    home = requests.get('https://en.autogidas.lt{}'.format(link))
    return BeautifulSoup(home.text, 'html.parser')


def convert_price(price):
    return int(price[:-1].replace(' ', ''))


def get_element(page, tag, class_name):
    element = page.find(tag, class_=class_name)
    return element.text.strip() if element is not None else ''


def progress(count, total, status=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
    sys.stdout.flush()
