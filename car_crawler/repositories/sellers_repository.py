class SellerRepository:
    def __init__(self):
        self.sellers = []

    def add(self, seller):
        self.sellers.append(seller)

    def update(self, seller):
        if seller.company_name != '':
            found = next((s for s in self.sellers if s.company_name == seller.company_name), None)
        else:
            found = next((s for s in self.sellers if s.phone == seller.phone), None)

        if found is None:
            self.add(seller)
        else:
            found.add_car(seller.summary_year, seller.summary_price)

    def get_all(self):
        return [s.get() for s in self.sellers]
