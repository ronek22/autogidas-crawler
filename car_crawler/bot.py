import csv
from multiprocessing.pool import ThreadPool

from tqdm import tqdm
from pathlib import Path
from car_crawler.models.seller import Seller
from car_crawler.repositories.sellers_repository import SellerRepository
from .utitlity import get_page, get_detail_page, convert_price, progress, get_element


class Bot:
    def __init__(self):
        self.sellers = SellerRepository()
        self.pages = 0
        self.cars_links = []

    def run(self):
        self._get_all_links()
        how_many = len(self.cars_links)
        print("ILOSC SAMOCHODOW: ", how_many)

        with ThreadPool(10) as pool:
            for _ in tqdm(pool.imap(self.handle_detail_page, self.cars_links), total=len(self.cars_links)):
                pass

        self.get_results()
        self.write_to_csv()

    def _get_all_links(self):
        self.first_page_info()

        with ThreadPool(4) as pool:
            for _ in tqdm(pool.imap(self.get_links_to_cars_from_page, list(range(1, self.pages))), total=self.pages):
                pass

    def first_page_info(self):
        page = get_page(1)
        paginator = page.find('div', class_='paginator')
        self.pages = int(paginator.find_all('a', href=True)[-2].text)

    def get_links_to_cars_from_page(self, index):
        page = get_page(index)
        container = page.find('div', class_='all-ads-block content-section')
        self.cars_links.extend(
            [a.get('href') for a in container.find_all('a', href=True) if "page" not in a.get('href')])

    def handle_detail_page(self, link):
        page = get_detail_page(link)
        try:
            company_name = get_element(page, 'span', 'seller-name')
            address = get_element(page, 'div', 'seller-ico seller-btn seller-location')
            price = convert_price(get_element(page, 'div', 'price'))
            year = int(page.find("div", text="Year").findNext('div').text[:4])
            phone = get_element(page, 'div', 'seller-ico seller-phones btn-action').split('\t', 1)[0]
            link_container = page.find('div', class_='seller-ico seller-btn seller-page-link seller-link')
            link = link_container.find_all('a', href=True)[0]['href'] if link_container is not None else ''
            seller = Seller(company_name, link, phone, year, price, address)
            self.sellers.update(seller)
        except Exception as e:
            print(e, link)

    def get_results(self):
        print(self.sellers.get_all())

    def write_to_csv(self):
        data = self.sellers.get_all()
        keys = data[0].keys()

        with open(Path("output", "result.csv"), "w", encoding='utf-8', newline='') as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(data)
